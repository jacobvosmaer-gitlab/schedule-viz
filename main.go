package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io"
	"log"
	"math/big"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

func main() {
	rand.Seed(time.Now().Unix())
	log.Fatal(http.ListenAndServe(":8080", http.HandlerFunc(handle)))
}

func runExperiment(avgSleep time.Duration, numProcs int, experimentLength time.Duration, offset time.Duration, algo string) (*big.Int, int, error) {
	// The main idea of Sidekiq's randomized sleep: multiply the intended
	// average sleep duration by the number of Sidekiq processes. Below, we
	// use float64's to represent durations in seconds so we must scale
	// avgSleep to seconds.
	scaledSleep := float64(avgSleep) / float64(time.Second) * float64(numProcs)

	var sleep func() int // returns a random sleep time in seconds
	switch algo {
	case "simple":
		// "simple" behaves as one would expect: no minimum sleep time and the
		// mean is scaledSleep.
		sleep = func() int {
			return int(2 * scaledSleep * rand.Float64())
		}
	case "sidekiq":
		// "sidekiq" implements the Sidekiq 6.4 algorithm: if numProcs is less
		// than 10 there is a minimum sleep time. If numProcs is 10 or more there
		// is no minimum sleep time, but the mean becomes scaledSleep/2.
		sleep = func() int {
			if numProcs < 10 {
				return int(scaledSleep/2 + rand.Float64()*scaledSleep)
			} else {
				return int(scaledSleep * rand.Float64())
			}
		}
	case "sidekiq-old":
		sleep = func() int {
			return int(scaledSleep/2 + rand.Float64()*scaledSleep)
		}
	default:
		return nil, 0, fmt.Errorf("algo: unknown sleep algorithm %s", algo)
	}

	bitmap := big.NewInt(0)
	n := int((offset + experimentLength) / time.Second)
	for j := 0; j < numProcs; j++ {
		for i := 0; i < n; i += sleep() {
			bitmap.SetBit(bitmap, i, 1)
		}
	}
	offsetInt := int(offset / time.Second)
	bitmap.Rsh(bitmap, uint(offsetInt))

	return bitmap, n - offsetInt, nil
}

func errorf(w io.Writer, format string, a ...interface{}) {
	fmt.Fprintf(w, "error: "+format, a...)
}

func render(data *big.Int, dataLen int) image.Image {
	rect := image.Rect(0, 0, dataLen, 6)
	img := image.NewRGBA(rect)

	const (
		rowLegendMin = 2
		rowLegendMax = 4
		rowData      = 1
	)

	for y := 0; y < rect.Dy(); y++ {
		for x := 0; x < rect.Dx(); x++ {
			img.Set(x, y, color.White)
		}
	}
	for x := 0; x < rect.Dx(); x += 60 {
		red := color.RGBA{0xff, 0, 0, 0xff}
		for y := rowLegendMin; y <= rowLegendMax; y++ {
			img.Set(x, y, red)
		}
	}

	for x := 0; x < dataLen; x++ {
		if data.Bit(x) == 1 {
			img.Set(x, rowData, color.Black)
		}
	}

	return img
}

func handle(rw http.ResponseWriter, r *http.Request) {
	avgSleep, err := time.ParseDuration(r.FormValue("avgSleep"))
	if err != nil {
		errorf(rw, "avgSleep: %v", err)
		return
	}
	numProcs, err := strconv.Atoi(r.FormValue("numProcs"))
	if err != nil {
		errorf(rw, "numProcs: %v", err)
		return
	}
	experimentLength, err := time.ParseDuration(r.FormValue("experimentLength"))
	if err != nil {
		errorf(rw, "experimentLength: %v", err)
		return
	}
	offset, err := time.ParseDuration(r.FormValue("offset"))
	if err != nil {
		offset = 0
	}

	data, dataLen, err := runExperiment(avgSleep, numProcs, experimentLength, offset, r.FormValue("algo"))
	if err != nil {
		errorf(rw, "runExperiment: %v", err)
		return
	}

	rw.Header().Set("content-type", "image/png")
	png.Encode(rw, render(data, dataLen))
}
